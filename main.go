package main

import (
	"time"

	"github.com/tosone/logging"
)

func main() {
	var conf = logging.Config{
		LogLevel:   logging.DebugLevel,
		Filename:   "/tmp/test.log",
		MaxSize:    10,
		MaxBackups: 2,
		MaxAge:     30,
		LocalTime:  true,
		Compress:   true,
	}
	logging.Setting(conf)
	for {
		logging.Infof("hello i am here %s", time.Now().String())
		<-time.After(time.Second)
	}
}
