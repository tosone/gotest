FROM cps-k8s.cargo.io/release/golang:alpine as build

WORKDIR /go/src/github.com/tosone/testgo/

COPY . .

RUN go build && ls && cp testgo /tmp/

FROM cps-k8s.cargo.io/release/alpine:3.12

WORKDIR /app

COPY --from=build /tmp/testgo /app

CMD /app/testgo
