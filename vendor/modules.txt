# github.com/fatih/color v1.10.0
## explicit
github.com/fatih/color
# github.com/mattn/go-colorable v0.1.8
github.com/mattn/go-colorable
# github.com/mattn/go-isatty v0.0.12
github.com/mattn/go-isatty
# github.com/tosone/logging v1.2.1
## explicit
github.com/tosone/logging
# golang.org/x/sys v0.0.0-20210309074719-68d13333faf2
## explicit
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
# gopkg.in/natefinch/lumberjack.v2 v2.0.0
gopkg.in/natefinch/lumberjack.v2
