module github.com/tosone/testgo

go 1.15

require (
	github.com/fatih/color v1.10.0 // indirect
	github.com/tosone/logging v1.2.1
	golang.org/x/sys v0.0.0-20210309074719-68d13333faf2 // indirect
)
